import React, {Component} from 'react';
import axios from "axios";

class App extends Component {

    state = {
        comments: [],
        users:[]
    };

    componentDidMount() {
        axios.get('https://jsonplaceholder.typicode.com/comments')
            .then((res) => {
                this.setState({
                    comments: res.data
                })
            });

        axios.get('https://jsonplaceholder.typicode.com/users')
            .then((res2) => {
                this.setState({
                    users: res2.data
                })
            });

    }

    render() {
        return (
            <div>
                {console.log(this.state.users)}
                <div className="container">
                    <div className="row my-5">
                        {this.state.users.map((item)=>(
                            <div className="col-md-4 mt-3" key={item.id}>
                                <div className="card">
                                    <div className="card-header text-center">
                                       <h4> {item.name}</h4>
                                    </div>
                                    <div className="card-body">
                                        <h6>Email: {item.email}</h6>
                                        <h6>Address: {item.address.city}</h6>
                                        <h6>Website: {item.website}</h6>
                                        <h6>Company: {item.company.name}</h6>
                                    </div>
                                </div>
                            </div>
                        ))}
                    </div>

                    <div className="row my-5">
                        <div className="col-md-12">
                            <table className="table table-dark table-striped">
                                <thead>
                                <tr>
                                    <th>TR</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Body</th>
                                </tr>
                                </thead>
                                <tbody>
                                {this.state.comments.map((item)=>(
                                    <tr key={item.id}>

                                        <td>{item.id}</td>
                                        <td>{item.name}</td>
                                        <td>{item.email}</td>
                                        <td>{item.body}</td>

                                    </tr>
                                ))}
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            </div>
        );
    }
}

export default App;